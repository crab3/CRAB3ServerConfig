from __future__ import division
from multiprocessing import cpu_count
from WMCore.Configuration import ConfigurationEx
import HTCondorLocator
from pprint import pprint
import logging

def totalRandom(collectorSchedds, goodSchedds, logger=None):
    """
        collectorSchedds: schedd objects list coming from the collector, the TW does a query and collect the statistics there
        goodSchedds: list of schedd the TW believes we should use (the ones from the rest/user config minus retry, we do not retry twice the same schedd)
        return: a list of tuples where the first element of the tuple is the schedd object and the second element is the weight for this schedd
    """
    choices = [(schedd["Name"], 1) for schedd in collectorSchedds if schedd["Name"] in goodSchedds]
    return choices

def tunedScheddSubmission2(schedds, goodSchedds, logger=None):
    """ Choose the schedd based on the TotalFreeMemoryMB classad present in the schedds object
        Return a list of scheddobj and the weight to be used in the weighted choice
    """
    scheddsToChoose = {}

    for sch in schedds:
        #schedd by schedd
        memPerc = sch['TotalFreeMemoryMB']/sch['DetectedMemory']                                                       # worst case TotalFreeMemoryMB=0
        uplPerc = (sch['TransferQueueMaxUploading']-sch['TransferQueueNumUploading'])/sch['TransferQueueMaxUploading'] # worst case TransferQueueNumUploading=TransferQueueMaxUploading
        jobPerc = (sch['MaxJobsRunning']-sch['TotalRunningJobs'])/sch['MaxJobsRunning']                                # worst case TotalRunningJobs=MaxJobsRunning
        #weight  = (memPerc + uplPerc + jobPerc)/3
        weight  = min(memPerc, uplPerc, jobPerc)
        # best option will be when we get 100% of free memory, 100% of uplods available and 100% of available jobs(no job running)
        # or ( memPerc + uplPerc + jobPerc )/3 = 1
        scheddsToChoose[sch['Name']] = weight

    choices = [(schedd, scheddsToChoose.get(schedd,0.5)) for schedd in goodSchedds]
    pprint(choices)

    return choices

def aNameJadirWillChoose(collectorSchedds, goodSchedds, logger=None):
    """
        collectorSchedds: schedd objects list coming from the collector, the TW does a query and collect the statistics there
        goodSchedds: list of schedd the TW believes we should use (the ones from the rest/user config minus retry, we do not retry twice the same schedd)
        return: a list of tuples where the first element of the tuple is the schedd object and the second element is the weight for this schedd
    """
    choices = HTCondorLocator.memoryBasedChoices(collectorSchedds, goodSchedds, logger)
    return choices

def newScheddPicker(schedds, goodSchedds, logger=None):
    """
    """
    # logger = logging.getLogger("master")

    choices = HTCondorLocator.capacityMetricsChoicesHybrid(schedds, goodSchedds, logger)
    return choices


config = ConfigurationEx()

## External services url's
config.section_("Services")
config.Services.PhEDExurl = 'https://phedex.cern.ch'
# config.Services.DBSUrl = 'https://cmsweb.cern.ch/dbs/prod/global/DBSReader'
config.Services.DBSUrl = 'https://vocms035.cern.ch/dbs/prod/global/DBSReader'
config.Services.MyProxy= 'myproxy.cern.ch'

config.section_("TaskWorker")
config.TaskWorker.polling = 20 #seconds
# we can add one worker per core, plus some spare ones since most of actions wait for I/O
config.TaskWorker.nslaves = 5
# config.TaskWorker.name = 'vocms0118' #Remember to update this!
config.TaskWorker.name = 'vocms058' #Remember to update this!
#config.TaskWorker.recurringActions = ['RenewRemoteProxies'] # RenewRemoteProxies
config.TaskWorker.recurringActions = ['RemovetmpDir', 'BanDestinationSites']
config.TaskWorker.scratchDir = '/data/srv/tmp' #make sure this directory exists

# Setting the list of users for the highprio accounting group.
config.TaskWorker.highPrioUsers = ['erupeika']

config.TaskWorker.XML_Report_ID = 'CMS_CRAB3_TaskWorker'

## Possible values for mode are:
#   - cmsweb-dev
#   - cmsweb-preprod
#   - cmsweb-prod
#   - private
config.TaskWorker.mode = 'private'
## If 'private' mode then a server url is needed
# config.TaskWorker.resturl = 'cmsweb.cern.ch'
config.TaskWorker.resturl = 'vocms035.cern.ch'
## the parameters here below are used to contact cmsweb services for the REST-DB interactions
config.TaskWorker.cmscert = '/data/certs/servicecert.pem'
config.TaskWorker.cmskey = '/data/certs/servicekey.pem'

config.TaskWorker.backend = 'glidein'
#Retry policy
config.TaskWorker.max_retry = 4
config.TaskWorker.retry_interval = [45, 90, 120, 0]


#Default False. If true dagman will not retry the job on ASO failures
config.TaskWorker.retryOnASOFailures = True
#Dafault 0. If -1 no ASO timeout, if transfer is stuck in ASO we'll retry the postjob FOREVER (well, eventually a dagman timeout for the node will be hit).
#If 0 default timeout of 4 to 6 hours will be used. If specified the timeout set will be used (minutes).
config.TaskWorker.ASOTimeout = 86400

# Control the ordering of stageout attempts.
# - remote means a copy from the worker node to the final destination SE directly.
# - local means a copy from the worker node to the worker node site's SE.
# One can include any combination of the above, or leaving one of the methods out.
# For example, CRAB2 is effectively:
# config.TaskWorker.stageoutPolicy = ["remote"]
# This is the CRAB3 default:
config.TaskWorker.stageoutPolicy = ["local", "remote"]
#config.TaskWorker.stageoutPolicy = ["remote"]
config.TaskWorker.dashboardTaskType = 'analysis'

# 0 - number of post jobs = max( (# jobs)*.1, 20)
# -1 - no limit
# This is needed for Site Metrics
# It should not block any site for Site Metrics and if needed for other activities
config.TaskWorker.ActivitiesToRunEverywhere = ['hctest', 'hcdev']

config.TaskWorker.maxPost = 20

# new schedd chooser 
#config.TaskWorker.scheddPickerFunction = HTCondorLocator.memoryBasedChoices
#config.TaskWorker.scheddPickerFunction = HTCondorLocator.capacityMetricsChoicesHybrid
config.TaskWorker.scheddPickerFunction = HTCondorLocator.capacityMetricsChoicesHybrid
#config.TaskWorker.scheddPickerFunction = tunedScheddSubmission2

config.TaskWorker.minAutomaticRuntime = 60

config.section_("Sites")
config.Sites.banned = []
#config.Sites.banned = ["T3_TW_NTU_HEP", "T2_RU_RRC_KI", "T2_TW_Taiwan", "T1_UK_RAL", "T2_RU_PNPI", "T3_RU_FIAN", "T3_US_UCD", "T3_US_NotreDame", "T3_US_Baylor", "T2_PL_Warsaw", "T2_BR_UERJ", "T1_TW_ASGC", "T2_GR_Ioannina", "T2_IN_TIFR", "T2_MY_UPM_BIRUNI", "T2_PK_NCP", "T2_PT_NCG_Lisbon", "T2_RU_ITEP", "T2_UA_KIPT" ]
# config.Sites.available = []
config.Sites.DashboardURL = 'https://cmssst.web.cern.ch/cmssst/analysis/usableSites.json'

config.section_("MyProxy")
config.MyProxy.serverhostcert = '/data/certs/hostcert.pem'
config.MyProxy.serverhostkey = '/data/certs/hostkey.pem'
#config.MyProxy.uisource = '/afs/cern.ch/cms/LCG/LCG-2/UI/cms_ui_env.sh'
config.MyProxy.cleanEnvironment = True
config.MyProxy.credpath = '/data/certs/creds' #make sure this directory exists
config.MyProxy.serverdn = '/DC=ch/DC=cern/OU=computers/CN=vocms058.cern.ch'


